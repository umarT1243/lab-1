import sys, pygame
import random
import math

## Setup code

pygame.init()

## Initialise all variables here

screen = pygame.display.set_mode((500,500))

x = 100
y = 100
dx = 0
dy = 0
game_over = False
success = False

# Randomly place the black target
target_x = random.randint(0, 500)
target_y = random.randint(0, 500)

aiming = True


# Game loop

while True:
    # Event detection
    # This is necessary for being able to close the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            x1, y1 = pos
            c1 = x - x1
            c2 = y - y1
            distance = math.sqrt(c1**2 + c2**2)
            if distance < 100:
                k = 10
                dx = c1 / k
                dy = c2 / k
                aiming = False
 

    # Update game model

    x += dx
    y += dy
    
    
    distance_to_target = math.sqrt((x - target_x)**2 + (y - target_y)**2)
    
    # Check if the game is won
    if distance_to_target < 10:
        game_over = True
        success = True

    # Bounce the ball off the walls
    if x > 500 or x < 0:
        dx = -dx
    if y > 500 or y < 0:
        dy = -dy
    
    # Slow down the ball
    dx *= 0.999
    dy *= 0.999
   

    # All drawing code goes here
    if success:
        screen.fill((0, 255, 0))
    else:
        screen.fill((0,0,0))
    
    for i in range(50):
        for j in range(50):
            pygame.draw.circle(screen, (0, 200, 0), (i * 10, j *10), 3)


    if not game_over:
        pygame.draw.circle(screen,(255,255,255),(x,y),10)
    

     # Draw the black target
    pygame.draw.circle(screen, (0, 0, 0), (target_x, target_y), 10)

    #aim area
    if aiming ==True:
        pygame.draw.circle(screen, (125, 125, 125), (x, y), 100, 5)

    pygame.display.update()

